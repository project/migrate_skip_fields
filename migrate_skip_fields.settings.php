<?php

/**
 * @file
 * Example settings for migrate_skip_fields module.
 */

// Drupal core version of source site. Either '6' or '7'.
$settings['migrate_skip_fields_source_version'] = '7';

// Skip by entity type, bundle, and field name. An asterisk serves as a
// wildcard to skip all values of one component.
$settings['migrate_skip_fields_by_name'] = [
  'entity_type:bundle:field_name',
  'entity_type:bundle:*',
  'entity_type:*:field_name',
  '*:*:field_name',
];

// Skip by field type. See hook_field_info. Alternatively, see the
// content_node_field table in Drupal 6 or the field_config table in Drupal 7.
$settings['migrate_skip_fields_by_type'] = [
  'field_type_1',
  'field_type_2',
];
