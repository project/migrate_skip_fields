<?php

namespace Drupal\migrate_skip_fields\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Site\Settings;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\migrate_drupal\FieldDiscoveryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a migrate_skip_fields_by_name plugin.
 *
 * @MigrateProcessPlugin(
 *   id = "migrate_skip_fields_by_name"
 * )
 */
class MigrateSkipFieldsByName extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  private const MIGRATE_SKIP_FIELDS_BY_NAME_MESSAGE = 'Skipped by migrate_skip_fields using migrate_skip_fields_by_name setting.';

  /**
   * The site settings.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * Constructs a MigrateSkipFieldsCheck plugin.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Site\Settings $settings
   *   The settings service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Settings $settings) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('settings')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function skipFieldStorage($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $migrate_skip_fields = $this->settings->get('migrate_skip_fields_by_name');
    $core_version = $this->settings->get('migrate_skip_fields_source_version', FieldDiscoveryInterface::DRUPAL_7);

    if (!\is_array($migrate_skip_fields)) {
      // @todo Throw exception.
      return $value;
    }

    $skip_fields_configuration = [];
    foreach ($migrate_skip_fields as $configuration) {
      [$skip_entity, $skip_bundle, $skip_field_name] = \explode(':', $configuration);
      $skip_fields_configuration[$skip_entity][$skip_bundle][] = $skip_field_name;
    }

    // @see \Drupal\migrate_drupal\FieldDiscovery::getAllFields
    // @see \Drupal\migrate_drupal\FieldDiscovery::$bundleKeys
    $entity_type = 'node';
    $bundle_key = 'type_name';
    if ($core_version === FieldDiscoveryInterface::DRUPAL_7) {
      $entity_type = $row->getSourceProperty('entity_type');
      $bundle_key = 'bundle';
    }

    $field_name = $value;

    // Remove the current field no matter the entity type or bundle it is
    // attached to.
    $global_field_excludes = $skip_fields_configuration['*']['*'] ?? [];
    if (\in_array($field_name, $global_field_excludes)) {
      throw new MigrateSkipRowException(self::MIGRATE_SKIP_FIELDS_BY_NAME_MESSAGE);
    }

    // The current field belongs to an entity that contains fields to be
    // removed.
    if (\in_array($entity_type, \array_keys($skip_fields_configuration))) {
      // All fields for this entity type should be removed no matter the bundle.
      if (\in_array('*', \array_keys($skip_fields_configuration[$entity_type]))) {
        throw new MigrateSkipRowException(self::MIGRATE_SKIP_FIELDS_BY_NAME_MESSAGE);
      }

      $instances = $row->getSourceProperty('instances');

      foreach ($instances as $index => $instance) {
        $instance_bundle = $instance[$bundle_key];

        // Check if there are fields in this bundle to be skipped.
        if (\in_array($instance_bundle, \array_keys($skip_fields_configuration[$entity_type]))) {

          // Unset if all fields for this bundle should be removed.
          if (\in_array('*', \array_values($skip_fields_configuration[$entity_type][$instance_bundle]))) {
            unset($instances[$index]);
          }

          // Unset if fields explicitly listed to be removed for this bundle.
          if (\in_array($field_name, \array_values($skip_fields_configuration[$entity_type][$instance_bundle]))) {
            unset($instances[$index]);
          }
        }
      }

      if (\count($instances) === 0) {
        throw new MigrateSkipRowException(self::MIGRATE_SKIP_FIELDS_BY_NAME_MESSAGE);
      }
    }

    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function skipFieldInstance($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $migrate_skip_fields = $this->settings->get('migrate_skip_fields_by_name');

    if (!\is_array($migrate_skip_fields)) {
      // @todo Throw exception.
      return $value;
    }

    $skip_fields_configuration = [];
    foreach ($migrate_skip_fields as $configuration) {
      [$skip_entity, $skip_bundle, $skip_field_name] = \explode(':', $configuration);
      $skip_fields_configuration[$skip_entity][$skip_bundle][] = $skip_field_name;
    }

    $entity_type = $row->getSourceProperty('entity_type');
    $bundle = $row->getSourceProperty('bundle');
    $field_name = $value;

    // Remove the current field no matter the entity type or bundle it is
    // attached to.
    $global_field_excludes = $skip_fields_configuration['*']['*'] ?? [];
    if (\in_array($field_name, $global_field_excludes)) {
      throw new MigrateSkipRowException(self::MIGRATE_SKIP_FIELDS_BY_NAME_MESSAGE);
    }

    // The current field belongs to an entity that contains fields to be
    // removed.
    if (\in_array($entity_type, \array_keys($skip_fields_configuration))) {
      // All fields for this entity type should be removed no matter the bundle.
      if (\in_array('*', \array_keys($skip_fields_configuration[$entity_type]))) {
        throw new MigrateSkipRowException(self::MIGRATE_SKIP_FIELDS_BY_NAME_MESSAGE);
      }

      if (\in_array($bundle, \array_keys($skip_fields_configuration[$entity_type]))) {
        // All fields for this entity/bundle combination should be removed no
        // matter the field name.
        if (\in_array('*', \array_values($skip_fields_configuration[$entity_type][$bundle]))) {
          throw new MigrateSkipRowException(self::MIGRATE_SKIP_FIELDS_BY_NAME_MESSAGE);
        }

        // This field name was configured to be removed for this entity/bundle
        // combination.
        if (\in_array($field_name, \array_values($skip_fields_configuration[$entity_type][$bundle]))) {
          throw new MigrateSkipRowException(self::MIGRATE_SKIP_FIELDS_BY_NAME_MESSAGE);
        }
      }
    }

    return $value;
  }

}
