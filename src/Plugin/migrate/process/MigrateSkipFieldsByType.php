<?php

namespace Drupal\migrate_skip_fields\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Site\Settings;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a migrate_skip_fields_by_type plugin.
 *
 * @MigrateProcessPlugin(
 *   id = "migrate_skip_fields_by_type"
 * )
 */
class MigrateSkipFieldsByType extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  private const MIGRATE_SKIP_FIELDS_UNSUPPORTED_TYPE_MESSAGE = 'Skipped by migrate_skip_fields. This field of type %s cannot be migrated because there is no MigrateField plugin that supports its type.';

  private const MIGRATE_SKIP_FIELDS_BY_TYPE_MESSAGE = 'Skipped by migrate_skip_fields using migrate_skip_fields_by_type setting. Field type: %s.';

  /**
   * The site settings.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * Constructs a MigrateSkipFieldsCheck plugin.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Site\Settings $settings
   *   The settings service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Settings $settings) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('settings')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $field_type = $value;

    $migrate_skip_fields_by_type = $this->settings->get('migrate_skip_fields_by_type');

    if (!\is_array($migrate_skip_fields_by_type)) {
      // @todo Throw exception.
      return $value;
    }

    // The current field type is configured to be skipped.
    if (\in_array($field_type, $migrate_skip_fields_by_type)) {
      throw new MigrateSkipRowException(\sprintf(self::MIGRATE_SKIP_FIELDS_BY_TYPE_MESSAGE, $field_type));
    }

    return $value;
  }

}
