This module allows skipping field by entity, bundle, name, or type. Refer to
`migrate_skip_fields.settings.php` for examples on how to configure the module.
